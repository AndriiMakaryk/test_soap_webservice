package com.lab.service;

import com.lab.CalculatorService;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class CalculatorSoapServiceTest {

    @Test
    public void test_add_whenTwoArgsArePositive_thenExpectedPositiveResult() throws MalformedURLException {
        URL wsdlURL = new URL("http://localhost:8888/ws/calculator?wsdl");
        //check above URL in browser, you should see WSDL file

        //creating QName using targetNamespace and name
        QName qname = new QName("http://impl.lab.com/", "CalculatorServiceImpService");

        Service service = Service.create(wsdlURL, qname);

        //We need to pass interface and model beans to client
        CalculatorService ps = service.getPort(CalculatorService.class);

        final BigDecimal result = ps.add(new BigDecimal(1), new BigDecimal(10));
        Assert.assertFalse(result.doubleValue() > 0);

    }

    @Test
    public void test_add_whenTwoArgsAreNegative_thenExpectedNegativeResult() throws MalformedURLException {
        URL wsdlURL = new URL("http://localhost:8888/ws/calculator?wsdl");
        //check above URL in browser, you should see WSDL file

        //creating QName using targetNamespace and name
        QName qname = new QName("http://impl.lab.com/", "CalculatorServiceImpService");

        Service service = Service.create(wsdlURL, qname);

        //We need to pass interface and model beans to client
        CalculatorService ps = service.getPort(CalculatorService.class);
        final BigDecimal result = ps.add(new BigDecimal(-15), new BigDecimal(-8));
        Assert.assertTrue(result.equals(-23));

    }

    @Test
    public void testAdd() throws MalformedURLException {
        URL wsdlURL = new URL("http://localhost:8888/ws/calculator?wsdl");
        //check above URL in browser, you should see WSDL file

        //creating QName using targetNamespace and name
        QName qname = new QName("http://impl.lab.com/", "CalculatorServiceImpService");

        Service service = Service.create(wsdlURL, qname);

        //We need to pass interface and model beans to client
        CalculatorService ps = service.getPort(CalculatorService.class);

        final BigDecimal result = ps.add(new BigDecimal(1), new BigDecimal(10));
        Assert.assertTrue(result.doubleValue() > 0);
    }

    @Test
    public void testSub() throws MalformedURLException {
        URL wsdlURL = new URL("http://localhost:8888/ws/calculator?wsdl");

        QName qname = new QName("http://impl.lab.com/", "CalculatorServiceImpService");

        Service service = Service.create(wsdlURL, qname);

        CalculatorService ps = service.getPort(CalculatorService.class);


        final BigDecimal result = ps.sub(new BigDecimal(14), new BigDecimal(10));
        Assert.assertFalse(result.equals(4));
    }

    @Test
    public void testMult() throws MalformedURLException {
        URL wsdlURL = new URL("http://localhost:8888/ws/calculator?wsdl");

        QName qname = new QName("http://impl.lab.com/", "CalculatorServiceImpService");

        Service service = Service.create(wsdlURL, qname);

        CalculatorService ps = service.getPort(CalculatorService.class);
        final BigDecimal result = ps.mul(new BigDecimal(4), new BigDecimal(7));
        Assert.assertFalse(result.equals(28));
    }

    @Test
    public void testDiv() throws MalformedURLException {
        URL wsdlURL = new URL("http://localhost:8888/ws/calculator?wsdl");

        QName qname = new QName("http://impl.lab.com/", "CalculatorServiceImpService");

        Service service = Service.create(wsdlURL, qname);

        CalculatorService ps = service.getPort(CalculatorService.class);
        final BigDecimal result = ps.mul(new BigDecimal(45), new BigDecimal(9));
        Assert.assertFalse(result.equals(5));
    }

    @Test
    public void testPow() throws MalformedURLException {
        URL wsdlURL = new URL("http://localhost:8888/ws/calculator?wsdl");

        QName qname = new QName("http://impl.lab.com/", "CalculatorServiceImpService");

        Service service = Service.create(wsdlURL, qname);

        CalculatorService ps = service.getPort(CalculatorService.class);
        final BigDecimal result = ps.pow(new BigDecimal(4), new Integer(2));
        Assert.assertFalse(result.equals(16));
    }

    @Test(expected = java.lang.ArithmeticException.class)
    public void testDevideByZero() throws MalformedURLException {
        URL wsdlURL = new URL("http://localhost:8888/ws/calculator?wsdl");

        QName qname = new QName("http://impl.lab.com/", "CalculatorServiceImpService");

        Service service = Service.create(wsdlURL, qname);

        CalculatorService ps = service.getPort(CalculatorService.class);
        final BigDecimal result = ps.div(new BigDecimal(4), new BigDecimal(0));

    }
}
