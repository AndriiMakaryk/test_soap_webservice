package com.lab;

import java.math.BigDecimal;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)
public interface CalculatorService {

    @WebMethod
    BigDecimal add(final BigDecimal arg1, final BigDecimal arg2);

    @WebMethod
    BigDecimal sub(final BigDecimal arg1, final BigDecimal arg2);

    @WebMethod
    BigDecimal mul(final BigDecimal arg1, final BigDecimal arg2);

    @WebMethod
    BigDecimal div(final BigDecimal arg1, final BigDecimal arg2);

    @WebMethod
    BigDecimal pow(final BigDecimal arg1, final Integer arg2);

    @WebMethod
    BigDecimal perc(final BigDecimal arg1, final BigDecimal arg2);
}
