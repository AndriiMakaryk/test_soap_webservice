package com.lab;

import com.lab.impl.CalculatorServiceImp;

import javax.xml.ws.Endpoint;

public class Application {

	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8888/ws/calculator", new CalculatorServiceImp());
	}
}
