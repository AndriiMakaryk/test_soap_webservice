package com.lab.impl;

import com.lab.CalculatorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import javax.jws.WebService;

@WebService(endpointInterface = "com.lab.CalculatorService")
public class CalculatorServiceImp implements CalculatorService {

    private static final Logger logger = LoggerFactory.getLogger(CalculatorServiceImp.class);

    public BigDecimal add(BigDecimal arg1, BigDecimal arg2) {
        logger.info("Call to add two digits: {} {}", arg1, arg2);
        return arg1.add(arg2);
    }

    public BigDecimal sub(BigDecimal arg1, BigDecimal arg2) {
        logger.debug("Call to subtraction two digits: {} {}", arg1, arg2);
        return arg1.subtract(arg2);
    }

    public BigDecimal mul(BigDecimal arg1, BigDecimal arg2) {
        logger.info("Call to multiply two digits: {} {}", arg1, arg2);
        return arg1.multiply(arg2);
    }

    public BigDecimal div(BigDecimal arg1, BigDecimal arg2) {
        logger.debug("Call to divide two digits: {} {}", arg1, arg2);
        return arg1.divide(arg2);
    }

    public BigDecimal pow(BigDecimal arg1, Integer arg2) {
        logger.info("Call to power of digit: {} {}", arg1, arg2);
        return arg1.pow(arg2);
    }

    public BigDecimal perc(BigDecimal arg1, BigDecimal arg2) {
        logger.debug("Call to percent correlation of two digits: {} {}", arg1, arg2);
        return arg1.divide(arg2);
    }
}
